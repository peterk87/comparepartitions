using System;
using System.Collections.Generic;

namespace ComparePartitions
{
    public class ComparePartitions
    {
        private readonly double _sid1;
        private readonly int _sid1Groups;
        private readonly double _sid1High;
        private readonly double _sid1Low;
        private readonly double _sid2;
        private readonly int _sid2Groups;
        private readonly double _sid2High;
        private readonly double _sid2Low;
        private double _a;
        private double _adjustedRand;
        private double _aw1;
        private double _aw1High;
        private double _aw1Low;
        private double _aw2;
        private double _aw2High;
        private double _aw2Low;
        private double _b;
        private double _c;
        private Dictionary<string, Dictionary<string, int>> _contTable = new Dictionary<string, Dictionary<string, int>>();

        private double _d;
        private List<string> _headers1 = new List<string>();
        private List<string> _headers2 = new List<string>();
        private int _n;
        private double _rand;
        private Dictionary<string, int> _sumCol = new Dictionary<string, int>();
        private Dictionary<string, int> _sumRow = new Dictionary<string, int>();
        private int _total;

        private double _w1;
        private double _w1High;
        private double _w1Low;
        private double _w2;
        private double _w2High;
        private double _w2Low;
        private double _wi1;
        private double _wi2;

        public ComparePartitions(string[] ar1, string[] ar2)
        {
            GetFullContTable(ar1, ar2);
            GetMisMatchMatrix();
            CalculateWallace();
            CalculateRand();
            CalculateAdjustedRand();
            CalculateSimpsonsIndexOfDiversity(ar1, out _sid1, out _sid1Low, out _sid1High, out _sid1Groups);
            CalculateSimpsonsIndexOfDiversity(ar2, out _sid2, out _sid2Low, out _sid2High, out _sid2Groups);
            CalculateAdjustedWallace();
        }

        public double W1Low { get { return _w1Low; } }
        public double W1High { get { return _w1High; } }
        public double W2Low { get { return _w2Low; } }
        public double W2High { get { return _w2High; } }

        public double SID2 { get { return _sid2; } }
        public int SID2Groups { get { return _sid2Groups; } }
        public double SID2High { get { return _sid2High; } }
        public double SID2Low { get { return _sid2Low; } }
        public double Aw1 { get { return _aw1; } }
        public double Aw1High { get { return _aw1High; } }
        public double Aw1Low { get { return _aw1Low; } }
        public double Aw2 { get { return _aw2; } }
        public double Aw2High { get { return _aw2High; } }
        public double Aw2Low { get { return _aw2Low; } }
        public double Wi1 { get { return _wi1; } }
        public double Wi2 { get { return _wi2; } }


        public double Sid1 { get { return _sid1; } }

        public double Sid1Low { get { return _sid1Low; } }

        public double Sid1High { get { return _sid1High; } }

        public int Sid1Groups { get { return _sid1Groups; } }


        public double A { get { return _a; } }

        public double B { get { return _b; } }

        public double C { get { return _c; } }

        public double D { get { return _d; } }

        public int N { get { return _n; } }

        public int Total { get { return _total; } }

        public Dictionary<string, int> SumRow { get { return _sumRow; } }

        public Dictionary<string, int> SumCol { get { return _sumCol; } }

        public List<string> Headers2 { get { return _headers2; } }

        public List<string> Headers1 { get { return _headers1; } }

        public double W1 { get { return _w1; } }

        public double W2 { get { return _w2; } }

        public double Rand { get { return _rand; } }

        public double AdjustedRand { get { return _adjustedRand; } }

        private void CalculateAdjustedWallace()
        {
            if (Math.Abs(_sid1 - 1.0) < double.Epsilon || Math.Abs(_sid2 - 1.0) < double.Epsilon)
            {
                _wi1 = 1.0 - _sid1;
                _wi2 = 1.0 - _sid2;
                if (Math.Abs(_sid1 - 1.0) < double.Epsilon)
                {
                    _w1 = 1.0;
                    _w1Low = 1.0;
                    _w1High = 1.0;
                    _w2 = 0.0;
                    _w2Low = 0.0;
                    _w2High = 0.0;
                    _aw1 = 1.0;
                    _aw1High = 1.0;
                    _aw1Low = 1.0;
                    _aw2 = 0.0;
                    _aw2High = 0.0;
                    _aw2Low = 0.0;
                }
                else
                {
                    _w1 = 0.0;
                    _w1Low = 0.0;
                    _w1High = 0.0;
                    _w2 = 1.0;
                    _w2Low = 1.0;
                    _w2High = 1.0;
                    _aw1 = 0.0;
                    _aw1High = 0.0;
                    _aw1Low = 0.0;
                    _aw2 = 1.0;
                    _aw2High = 1.0;
                    _aw2Low = 1.0;
                }
                return;
            }


            double rSumW1 = 0.0;
            double rSumW2 = 0.0;
            var csumFc2 = new Dictionary<string, double>();
            var csumFc3 = new Dictionary<string, double>();
            foreach (var pair1 in _contTable)
            {
                string i = pair1.Key;
                var rSum = (double) _sumCol[i];
                double rsumFc2 = 0.0;
                double rsumFc3 = 0.0;
                foreach (var pair2 in pair1.Value)
                {
                    string j = pair2.Key;
                    var val = (double) _contTable[i][j];
                    int cSum = _sumRow[j];
                    rsumFc2 += Math.Pow(val / (rSum), 2.0);
                    rsumFc3 += Math.Pow(val / (rSum), 3.0);
                    if (csumFc2.ContainsKey(j))
                        csumFc2[j] += Math.Pow(val / (cSum), 2.0);
                    else
                        csumFc2[j] = Math.Pow(val / (cSum), 2.0);
                    if (csumFc3.ContainsKey(j))
                        csumFc3[j] += Math.Pow(val / (cSum), 3.0);
                    else
                        csumFc3[j] = Math.Pow(val / (cSum), 3.0);
                }
                double rvarSID = 0.0;
                if (rSum > 1.0)
                    rvarSID = GetVarSID(rSum, rsumFc2, rsumFc3);
                rSumW1 += Math.Pow((rSum * (rSum - 1.0)), 2.0) * rvarSID;
                rSumW2 += rSum * (rSum - 1.0);
            }
            double csumw1 = 0.0;
            double csumW2 = 0.0;

            foreach (var pair in _sumRow)
            {
                string j = pair.Key;
                int cSum = _sumRow[j];
                double fc2 = csumFc2[j];
                double fc3 = csumFc3[j];
                double cvarSID = 0.0;
                if (cSum > 1.0)
                    cvarSID = GetVarSID(cSum, fc2, fc3);
                csumw1 += Math.Pow(cSum * (cSum - 1.0), 2.0) * cvarSID;
                csumW2 += cSum * (cSum - 1.0);
            }
            // get variance of Wallace 1vs2 (varW1) and 2vs1 (varW2);
            double varW1 = 0.0;
            double varW2 = 0.0;
            if (rSumW2 > 0.0)
                varW1 = (rSumW1 / Math.Pow((rSumW2), 2.0));
            if (csumW2 > 0.0)
                varW2 = (csumw1 / Math.Pow((csumW2), 2.0));
            // get mismatch matrix;
            double w1 = _w1;
            double w2 = _w2;

            _wi1 = 1.0 - _sid1;
            _wi2 = 1.0 - _sid2;

            _aw1 = (w1 - _wi2) / (1.0 - _wi2);
            _aw2 = (w2 - _wi1) / (1.0 - _wi1);

            double aw1CI = 2.0 * (1.0 / (1.0 - _wi2)) * Math.Sqrt(varW1);
            _aw1Low = _aw1 - aw1CI;
            if (_aw1Low < 0.0)
                _aw1Low = 0.0;
            _aw1High = _aw1 + aw1CI;
            if (_aw1High > 1.0)
                _aw1High = 0.0;

            double aw2CI = 2.0 * (1.0 / (1.0 - _wi1)) * Math.Sqrt(varW2);
            _aw2Low = _aw2 - aw2CI;
            if (_aw2Low < 0.0)
                _aw2Low = 0.0;
            _aw2High = _aw2 + aw2CI;
            if (_aw2High > 1.0)
                _aw2High = 0.0;


            double w1CI = 2.0 * Math.Sqrt(varW1);
            _w1Low = _w1 - w1CI;
            if (_w1Low < 0.0)
                _w1Low = 0.0;
            _w1High = _w1 + w1CI;
            if (_w1High > 1.0)
                _w1High = 1.0;

            double w2CI = 2.0 * Math.Sqrt(varW1);
            _w2Low = _w2 - w2CI;
            if (_w2Low < 0.0)
                _w2Low = 0.0;
            _w2High = _w2 + w2CI;
            if (_w2High > 1.0)
                _w2High = 1.0;
        }

        private void GetFullContTable(string[] ar1, string[] ar2)
        {
            _contTable = ContTable(ar1, ar2);
            GetContTableHeaders(_contTable, ref _headers1, ref _headers2);
            GetContTableTotals(_contTable, _headers1, _headers2, ref _sumCol, ref _sumRow, out _total);
        }

        /// <summary>Get the contigency table for frequency of different values for one array vs another array of int values.</summary>
        /// <param name="ar1">Array of ints</param>
        /// <param name="ar2">Array of ints</param>
        /// <returns>Contigency table of counts for frequency of different values.</returns>
        private static Dictionary<string, Dictionary<string, int>> ContTable(string[] ar1, string[] ar2)
        {
            var cont = new Dictionary<string, Dictionary<string, int>>();

            for (int i = 0; i < ar1.Length; i++)
            {
                string keyArray1 = ar1[i];
                string keyArray2 = ar2[i];
                if (cont.ContainsKey(keyArray1))
                {
                    if (cont[keyArray1].ContainsKey(keyArray2))
                    {
                        cont[keyArray1][keyArray2]++;
                    }
                    else
                    {
                        cont[keyArray1].Add(keyArray2, 1);
                    }
                }
                else
                {
                    cont.Add(keyArray1, new Dictionary<string, int> {{keyArray2, 1}});
                }
            }
            return cont;
        }

        private static void GetContTableHeaders(Dictionary<string, Dictionary<string, int>> cont,
                                                ref List<string> headers1,
                                                ref List<string> headers2)
        {
            var uniqueHeaders2 = new HashSet<string>();
            foreach (string key1 in cont.Keys)
            {
                headers1.Add(key1);
                foreach (string key2 in cont[key1].Keys)
                {
                    uniqueHeaders2.Add(key2);
                }
            }
            headers2.AddRange(uniqueHeaders2);
        }

        private static void GetContTableTotals(Dictionary<string, Dictionary<string, int>> cont,
                                               List<string> headers1,
                                               List<string> headers2,
                                               ref Dictionary<string, int> sumCol,
                                               ref Dictionary<string, int> sumRow,
                                               out int total)
        {
            foreach (string header2 in headers2)
            {
                sumRow.Add(header2, 0);
                foreach (string header1 in headers1)
                {
                    if (!cont[header1].ContainsKey(header2)) continue;
                    int val = cont[header1][header2];
                    sumRow[header2] += val;
                    if (sumCol.ContainsKey(header1))
                    {
                        sumCol[header1] += val;
                    }
                    else
                    {
                        sumCol.Add(header1, val);
                    }
                }
            }
            total = 0;
            foreach (string header1 in headers1)
            {
                total += sumCol[header1];
            }
        }

        private void GetMisMatchMatrix()
        {
            _a = 0.0;
            foreach (string header1 in _headers1)
            {
                foreach (string header2 in _headers2)
                {
                    if (!_contTable[header1].ContainsKey(header2)) continue;
                    int val = _contTable[header1][header2];
                    _a += (val * (val - 1d)) / 2d;
                }
            }
            double a1 = 0.0;
            foreach (int val in _sumCol.Values)
            {
                a1 += (val * (val - 1d)) / 2d;
            }
            _b = a1 - _a;

            double a2 = 0.0;
            foreach (int val in _sumRow.Values)
            {
                a2 += (val * (val - 1d)) / 2d;
            }
            _c = a2 - _a;

            _n = _total;
            _d = ((_n * (_n - 1d)) / 2d) - a1 - _c;
        }

        private void CalculateRand()
        {
            _rand = (_a + _d) / (_a + _b + _c + _d);
        }

        private void CalculateAdjustedRand()
        {
            _adjustedRand = (((_n * (_n - 1)) / 2d) * (_a + _d) - ((_a + _b) * (_a + _c) + (_c + _d) * (_b + _d))) /
                            (Math.Pow((_n * (_n - 1)) / 2d, 2d) - ((_a + _b) * (_a + _c) + (_c + _d) * (_b + _d)));
        }

        private void CalculateWallace()
        {
            if (_a + _b > 0)
            {
                _w1 = _a / (_a + _b);
            }
            else
            {
                _w1 = 0;
            }

            if (_a + _c > 0)
            {
                _w2 = _a / (_a + _c);
            }
            else
            {
                _w2 = 0;
            }
        }

        private static double GetVarSID(double cSum, double fc2, double fc3)
        {
            return (4.0 * cSum * (cSum - 1.0) * (cSum - 2.0) * fc3 + 2.0 * cSum * (cSum - 1.0) * fc2 -
                    2.0 * cSum * (cSum - 1.0) * (2.0 * cSum - 3.0) * (fc2 * fc2)) / ((cSum * (cSum - 1.0)) * (cSum * (cSum - 1.0)));
        }


        public static void CalculateSimpsonsIndexOfDiversity(string[] ar,
                                                             out double sid,
                                                             out double sidLow,
                                                             out double sidHigh,
                                                             out int sidGroups)
        {
            int n = ar.Length;
            var dict = new Dictionary<string, int>();
            foreach (string s in ar)
            {
                if (dict.ContainsKey(s))
                {
                    dict[s]++;
                }
                else
                {
                    dict.Add(s, 1);
                }
            }

            int sumTotal = 0;
            double sumFc2 = 0;
            double sumFc3 = 0;

            foreach (int i in dict.Values)
            {
                sumTotal += (i * (i - 1));
                sumFc2 += Math.Pow(i / (double) n, 2d);
                sumFc3 += Math.Pow(i / (double) n, 3d);
            }

            if ((n * (n - 1)) > 0)
            {
                sid = 1d - (sumTotal / (double) (n * (n - 1)));
            }
            else
            {
                sid = 1;
            }

            double sqSumFc2 = Math.Pow(sumFc2, 2d);
            double s2 = (4 / (double) n) * (sumFc3 - sqSumFc2);

            sidLow = sid - 2 * Math.Sqrt(s2);
            sidHigh = sid + 2 * Math.Sqrt(s2);

            sidGroups = dict.Count;
        }
    }
}