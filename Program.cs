﻿using System;
using System.Collections.Generic;
using System.IO;
using NDesk.Options;

namespace ComparePartitions
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            string output = "-";
            bool showHelp = false;
            var p = new OptionSet
                        {
                            {"o|out=", "Output filename. Default stdout.", s => output = s},
                            {"h|help=", "show this message and exit", s => showHelp = true},
                        };
            string inputFile;
            try
            {
                List<string> extra = p.Parse(args);
                if (extra.Count == 0)
                {
                    ShowHelp(p);
                    return;
                }
                inputFile = extra[0];
                if (!File.Exists(inputFile))
                    throw new OptionException("Input file does not exist.","");
            }
            catch (OptionException e)
            {
                Console.Write("ComparePartitions: ");
                Console.WriteLine(e.Message);
                Console.WriteLine();
                ShowHelp(p);
                return;
            }


            if (showHelp || args.Length == 0)
            {
                ShowHelp(p);
                return;
            }

            var headers = new List<string>();
            var clusters = new List<List<string>>();
            using (var sr = new StreamReader(inputFile))
            {
                string h = sr.ReadLine();
                if (string.IsNullOrEmpty(h))
                    return;
                headers.AddRange(h.Split('\t'));
                for (int i = 0; i < headers.Count; i++)
                {
                    clusters.Add(new List<string>());
                }
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    if (string.IsNullOrEmpty(line))
                        continue;
                    string[] lineSplit = line.Split('\t');
                    for (int i = 0; i < lineSplit.Length; i++)
                    {
                        clusters[i].Add(lineSplit[i]);
                    }
                }
            }
            var partitionsDict = new Dictionary<string, Dictionary<string, ComparePartitions>>();
            for (int i = 0; i < headers.Count - 1; i++)
            {
                string hI = headers[i];
                for (int j = i + 1; j < headers.Count; j++)
                {
                    string hJ = headers[j];
                    if (partitionsDict.ContainsKey(hI))
                    {
                        if (!partitionsDict[hI].ContainsKey(hJ))
                        {
                            partitionsDict[hI].Add(hJ, new ComparePartitions(clusters[i].ToArray(), clusters[j].ToArray()));
                        }
                    }
                    else
                    {
                        partitionsDict.Add(hI,
                                           new Dictionary<string, ComparePartitions>
                                               {{hJ, new ComparePartitions(clusters[i].ToArray(), clusters[j].ToArray())}});
                    }
                }
            }


            var sw = output == "-" ? new StreamWriter(Console.OpenStandardOutput()) : new StreamWriter(output);

            sw.WriteLine(string.Join("\t",
                                     "A",
                                     "B",
                                     "SID A",
                                     "SID B",
                                     "Rand",
                                     "Adjusted Rand",
                                     "Wallace",
                                     "Lower W",
                                     "Upper W",
                                     "Wi",
                                     "Adjusted Wallace",
                                     "Lower AW",
                                     "Upper AW"));
            foreach (var pair1 in partitionsDict)
            {
                string methodA = pair1.Key;
                foreach (var pair2 in partitionsDict[methodA])
                {
                    string methodB = pair2.Key;
                    ComparePartitions cp = pair2.Value;

                    string aVsB = string.Join("\t",
                                              methodA,
                                              methodB,
                                              cp.Sid1,
                                              cp.SID2,
                                              cp.Rand,
                                              cp.AdjustedRand,
                                              cp.W1,
                                              cp.W1Low,
                                              cp.W1High,
                                              1.0 - cp.SID2,
                                              cp.Aw1,
                                              cp.Aw1Low,
                                              cp.Aw1High);
                    string bVsA = string.Join("\t",
                                              methodB,
                                              methodA,
                                              cp.SID2,
                                              cp.Sid1,
                                              cp.Rand,
                                              cp.AdjustedRand,
                                              cp.W2,
                                              cp.W2Low,
                                              cp.W2High,
                                              1.0 - cp.Sid1,
                                              cp.Aw2,
                                              cp.Aw2Low,
                                              cp.Aw2High);
                    sw.WriteLine(aVsB);
                    sw.WriteLine(bVsA);
                }
            }
            sw.Close();
        }

        private static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: ComparePartitions [OPTIONS]+ <Clusters Table File>");
            Console.WriteLine(@"
Calculate table of partition congruence coefficients for a set of classifications. 
Rand, Adjusted Rand, Wallace, Adjusted Wallace, Simpson's Index of Diversity");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }
    }
}